package Test;

import Test.Report;

public class Login extends Browser {
	Report rpt = new Report();
	 public String[] cred = { "admin", "Cyber2013" };

	public void login() throws Exception {
		//Login form Check
		if (findTheElement("xpath=//*[@class=\"well form-horizontal\"]").isDisplayed()) {
			rpt.createTest("MB- Login Page -Check Login_form", "Login_form is Displayed - For Check Login_form");
			rpt.Pass("Login_form Displayed - For Check Login_form");
			rpt.Category("MB - Login Page -Check Login_form");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);

		} else {
			rpt.createTest("MB- Login Page - Check Login_form", "Login_form is NOT Displayed - For Check Login_form");
			rpt.Fail("Login_form is NOT Displayed - For Check Login_form");
			rpt.Category("MB- Login Page -Check Login_form");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
		}
		click("xpath=//*[@id=\"id_username\"]");
		sendKeys("xpath=//*[@id=\"id_username\"]", this.cred[0]);
		click("xpath=//*[@id=\"id_password\"]");
		sendKeys("xpath=//*[@id=\"id_password\"]", this.cred[1]);
		click("xpath=//button[@class=\"btn btn-primary\"]");
	}
}
