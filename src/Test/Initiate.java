package Test;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class Initiate {

	ExtentReports extent;
	ExtentTest test;
	public static WebDriver browser = BrowserFactory.getBrowser("browser", "url");
	BrowserFactory br = new BrowserFactory();
	Report rpt = new Report();
	WebDriver wb;

	@BeforeClass
	public void Ini() {
		extent = Report.GetExtent();

	}

	@SuppressWarnings("static-access")
	@Parameters({ "browser", "url", "Module" })
	@Test
	public void checkHome(String Browser, String Url, String Module) throws Exception {

		rpt.createTest("MAYER BROWN", "Verify HomePage");
		br.getBrowser(Browser, Url);
		rpt.Pass("Browser that is passed as parameter from Jenkins is :" + Browser);
		rpt.Pass("URL that is passed as parameter from Jenkins is :" + Url);
		// test.log(Status.PASS,"Browser Opened As Expected");
		// br.captureScreenShot("HomePage");

		if (Module != null) {
			login();
			if ("Company Search".equalsIgnoreCase(Module)) {
				chckCompanySearch();
			} else if ("People Search".equalsIgnoreCase(Module)) {
				chckPeopleSearch();
			} else if ("All Modules".equalsIgnoreCase(Module)) {
				chckCompanySearch();
				chckPeopleSearch();
				
			}

		}

	}

	public void login() throws Exception {
		Login signin = new Login();
		signin.login();
	}
	
	public void logout() throws Exception {
		Logout lg = new Logout();
		lg.logout();
	}

	public void chckCompanySearch() throws Exception {
		CompanySearch CompSearch = new CompanySearch();
		CompSearch.executeAll();
	}

	public void chckPeopleSearch() throws Exception {
		PeopleSearch PplSearch = new PeopleSearch();
		PplSearch.ExecuteAll();
	}

	public void checkFail() throws Exception {
		rpt.createTest("Testing - Fail Criteria for MAYER BROWN", "Checked");
		// test.log(Status.FAIL, "fail check started");
		rpt.Fail("Testing Fail - MAYER BROWN");

	}

	public void skipTest() {
		rpt.createTest("SkipTest", "Skipped");
		// throw new SkipException("Skipping Test");
		rpt.Skip("Test Skipped");
	}

	@AfterTest
	public void publishReport() throws Exception {
		extent.flush();

	}

}
