package Test;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.Select;

public class PeopleSearch extends Browser {
	
	Report rpt = new Report();
	
	public void ExecuteAll() throws Exception
	{
		MB_FieldAvailability();
		Clearall();
		selectall();
		MB_APISerachWithoutInput();
		MB_APISerachWithInput();
		Mb_ValidDunsNumber();
		DB_InvalidDuns();
		MB_DbSearchwithInput();
		MB_DbSearchwithoutInput();
		Db_Filter_Country();
		Db_Filter_StateProvince();
		Db_Filter_Title();
		
		DB_CompanyDetails(); 
		//MB_FinancialHistory();
	}
	
	public void MB_APISerachWithoutInput() throws Exception
	{
		//click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
		if (findTheElement(
				 "xpath=html/body/div[1]/div/div[3]/ul/li[2]/a")
					.isDisplayed()) {
				rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:People search tab is clicked");
				rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - People Search link is clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
				

			} else {
				rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:People search tab is NOT clicked");
				rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search link is NOT clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				
			}
		//click("xpath=//button[@id='api_search']");
				if (findTheElement(
						 "xpath=//button[@id='api_search']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:API Search button is Clicked");
						rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - API Search button is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//button[@id='api_search']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:API Search button is not Clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - API Search button is NOT Clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				Thread.sleep(5000);
		
		
		String Result=findTheElement("xpath=//div[@id='client_results_info']").getText();
		
		
		if(Result.contains("entries"))
		{
			rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:People search (API)  is working fine for without any input");
			rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - People Search is working fine with no input");
			rpt.Category("MAYER BROWN_PEOPLE SEARCH");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
		}
		
		else
		{
			rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is Not displayed", "Message:People search (API)  is NOT working fine for without any input");
			rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search is NOT working fine with no input");
			rpt.Category("MAYER BROWN_PEOPLE SEARCH");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
		}
		
		
	}
	
	
	public void MB_APISerachWithInput() throws Exception
	{
		//click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
		if (findTheElement(
				 "xpath=html/body/div[1]/div/div[3]/ul/li[2]/a")
					.isDisplayed()) {
				rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:People search tab is clicked");
				rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - People Search link is clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
				

			} else {
				rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:People search tab is NOT clicked");
				rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search link is NOT clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				
			}
		
		
		//sendKeys("xpath=//input[@id='id_first_name']","Kani");
		if (findTheElement(
				 "xpath=//input[@id='id_first_name']")
					.isDisplayed()) {
				rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:First Name is entered");
				rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - First Name is entered");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				sendKeys("xpath=//input[@id='id_first_name']","Kani");
				

			} else {
				rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:First Name is NOT Entered");
				rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - First name is NOT entered");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				
			}
		
		//sendKeys("xpath=//input[@id='id_last_name']","");
		//sendKeys("xpath=//input[@id='id_company_name']","");
		//sendKeys("xpath=//textarea[@id='id_keywords']","");
		//sendKeys("xpath=//textarea[@id='id_duns_number']","");
		//sendKeys("xpath=//input[@id='id_zipcode']","");
		//click("xpath=//button[@id='api_search']");
		if (findTheElement(
				 "xpath=//button[@id='api_search']")
					.isDisplayed()) {
				rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:API Search button is Clicked");
				rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - API Search button is clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				click("xpath=//button[@id='api_search']");
				

			} else {
				rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:API Search button is not Clicked");
				rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - API Search button is NOT Clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				
			}
		
		
		
		Thread.sleep(5000);
		
		String s=findTheElement("xpath=//div[@id='client_results_info']").getText();
		System.out.println(s);
		
		if(s.contains("entries"))
		{
			rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:People search (API)  is working fine for with input");
			rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - People Search is working fine with input");
			rpt.Category("MAYER BROWN_PEOPLE SEARCH");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
		}
		
		else
		{
			rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is Not displayed", "Message:People search (API)  is NOT working fine for with input");
			rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search is NOT working fine with input");
			rpt.Category("MAYER BROWN_PEOPLE SEARCH");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
		}
		
	}
	
	
	public void Mb_ValidDunsNumber() throws Exception
	{
		MB_APISerachWithoutInput();
		Thread.sleep(5000);
		//getting Dynamic DUNS Number
		String dunsnumber=findTheElement("xpath=.//*[@id='client_results']/tbody/tr[1]/td[4]").getText();
		browser.navigate().back();
		
		//sendKeys("xpath=//textarea[@id='id_duns_number']",dunsnumber);
		if (findTheElement(
				 "xpath=//textarea[@id='id_duns_number']")
					.isDisplayed()) {
				rpt.createTest("MAYER BROWN_People Search(API) - Verify DUNS Number Validation -  People Search page is displayed", "Message: DUNS Number is Entered");
				rpt.Pass("MAYER BROWN - People Search(API)- Message: DUNS Number is Entered");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				sendKeys("xpath=//textarea[@id='id_duns_number']",dunsnumber);
				

			} else {
				rpt.createTest("MAYER BROWN - Verify People Search(API)-  Verify DUNS Number Validation -  People Search page is displayed", "Message: DUNS Number is NOT Entered");
				rpt.Fail("MAYER BROWN - People Search(API)- Message: DUNS Number is NOT Entered");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				
			}
		
		//click("xpath=//button[@id='api_search']");
		if (findTheElement(
				 "xpath=//button[@id='api_search']")
					.isDisplayed()) {
				rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:API Search button is Clicked");
				rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - API Search button is clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				click("xpath=//button[@id='api_search']");
				

			} else {
				rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:API Search button is not Clicked");
				rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - API Search button is NOT Clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				
			}
		Thread.sleep(30000);
		String SearchResultDunsNumber=findTheElement("xpath=.//*[@id='client_results']/tbody/tr[1]/td[4]").getText();
		
		if (dunsnumber.equals(SearchResultDunsNumber))
		{
			rpt.createTest("MAYER BROWN_People Search(API) - Verify DUNS Number Validation -  People Search page is displayed", "Message: Page is working fine with valid DUNS Number ");
			rpt.Pass("MAYER BROWN - People Search(API)- Message: Page is working fine with valid DUNS Number");
			rpt.Category("MAYER BROWN_PEOPLE SEARCH");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);

		}
		else
		{
			rpt.createTest("MAYER BROWN_People Search(API) - Verify DUNS Number Validation -  People Search page is displayed", "Message: Page is NOT working fine with valid DUNS Number ");
			rpt.Fail("MAYER BROWN - People Search(API)- Message: Page is NOT working fine with valid DUNS Number");
			rpt.Category("MAYER BROWN_PEOPLE SEARCH");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
		}
		
	}
	
	public void DB_InvalidDuns() throws Exception
	{
		//click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
				if (findTheElement(
						 "xpath=html/body/div[1]/div/div[3]/ul/li[2]/a")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:People search tab is clicked");
						rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - People Search link is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:People search tab is NOT clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search link is NOT clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
		//Invalid Dunds
				//sendKeys("xpath=//textarea[@id='id_duns_number']","TEST");
				if (findTheElement(
						 "xpath=//textarea[@id='id_duns_number']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:Invalid DUNS Number  is Entered");
						rpt.Pass("MAYER BROWN - People Search(API)- Invalid DUNS Number is entered");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						sendKeys("xpath=//textarea[@id='id_duns_number']","TEST");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message: Invalid DUNS Number is not Entered ");
						rpt.Fail("MAYER BROWN - People Search(API)- Invalid DUNS Number is NOT Entered");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
						
					}
				
				//click("xpath=//button[@id='db_search']");
				if (findTheElement(
						 "xpath=//button[@id='db_search']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Search is working fine with no inpu");
						rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - API Search button is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//button[@id='db_search']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:API Search button is not Clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - API Search button is NOT Clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
				String ValidationErrorMessage=findTheElement("xpath=html/body/ul/li/ul/li").getText();
				System.out.println(ValidationErrorMessage);
				
				if(ValidationErrorMessage.contains("Enter a whole number."))
				{
					rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Search DUNS Number validation is working fine");
					rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link - DB Search DUNS Number validation is working fine");
					rpt.Category("MAYER BROWN_PEOPLE SEARCH");
					String path = rpt.CaptureScreen(browser, "ValidMessage");
					rpt.imgPathPass(path);
					
				}
				else
				{
					rpt.createTest("MAYER BROWN - Verify People Search(DB)-   People Search page is NOT displayed", "Message:DB Search DUNS Number validation is NOT working fine");
					rpt.Fail("MAYER BROWN - People Search(DB)-DB Search DUNS Number validation is NOT working fine");
					rpt.Category("MAYER BROWN_PEOPLE SEARCH");
					String path = rpt.CaptureScreen(browser, "InvalidMessage");
					rpt.imgPathFail(path);
					
				}
				
				browser.navigate().back();
			
				
				
				
	}
	
	
	
	public void Clearall() throws Exception
	{
		//click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
		if (findTheElement(
				 "xpath=html/body/div[1]/div/div[3]/ul/li[2]/a")
					.isDisplayed()) {
				rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:People search tab is clicked");
				rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - People Search link is clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
				

			} else {
				rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:People search tab is NOT clicked");
				rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search link is NOT clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				
			}
		
		//click("xpath=//button[@id='jf_clear_all']");
		if (findTheElement(
				 "xpath=//button[@id='jf_clear_all']")
					.isDisplayed()) {
				rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:Clear All button is clicked");
				rpt.Pass("MAYER BROWN - People Search(API)-Clear All button is clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				click("xpath=//button[@id='jf_clear_all']");
				

			} else {
				rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:Clear All button is NOT clicked");
				rpt.Fail("MAYER BROWN - People Search(API)- Clear All button is NOT clicked");
				rpt.Category("MAYER BROWN_PEOPLE SEARCH");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				
			}
		
		
		Map<String,String> ids= new HashMap<String,String>();
		 ids.put("Relavance", "id_job_function"); 
		 
		 
		                   
		 for(Map.Entry<String, String> id : ids.entrySet() )
		 {
			 if(findTheElement("xpath=//*[@id='"+id.getValue()+"']").isSelected())
			 {
				 rpt.createTest("MayerBrown - People Search  - Verify Relevance Drop Down option displayed in PLP ", "Message :"+id.getValue()+"is NOT Displayed in PLP Relevance Drop Down");
	              rpt.Fail("Message :"+id.getValue()+" All options in Job function checkbox selected");
	              rpt.Category("MayerBrown - People Search");
	              String path = rpt.CaptureScreen(browser, "InvalidMessage");
	              rpt.imgPathFail(path);
	              
	    
			 }
			 
			 else
			 {
				 rpt.createTest("MayerBrown - People Search   - Verify Job Function -Check Box ", "Message :"+id.getKey()+" Job Function field All options check box selected");
	              rpt.Pass("Message :"+id.getKey()+" All options in Job function checkbox selected ");
	              rpt.Category("MayerBrown_People Search");
	              String path = rpt.CaptureScreen(browser, "ValidMessage");
	              rpt.imgPathPass(path);
				  

			 }
		 
		 }
	}
		 

	public void selectall() throws Exception
			{
				//click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
				if (findTheElement(
						 "xpath=html/body/div[1]/div/div[3]/ul/li[2]/a")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:People search tab is clicked");
						rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - People Search link is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:People search tab is NOT clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search link is NOT clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
				
				//click("xpath=//button[@id='jf_select_all']");
				if (findTheElement(
						 "xpath=//button[@id='jf_clear_all']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message: Select All button is clicked");
						rpt.Pass("MAYER BROWN - People Search(API)- Select All button is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//button[@id='jf_select_all']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is displayed", "Message:Select All button is NOT clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Select All button is NOT clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
				
				
				Map<String,String> ids= new HashMap<String,String>();
				 ids.put("Job Function", "id_job_function"); 
				 
				 
				                   
				 for(Map.Entry<String, String> id : ids.entrySet() )
				 {
					 if(findTheElement("xpath=//*[@id='"+id.getValue()+"']").isEnabled())
					 {
						 rpt.createTest("MayerBrown - People Search   - Verify Job Function -Check Box ", "Message :"+id.getKey()+" Job Function field All options check box selected");
			              rpt.Pass("Message :"+id.getKey()+" All options in Job function checkbox selected ");
			              rpt.Category("MayerBrown_People Search");
			              String path = rpt.CaptureScreen(browser, "ValidMessage");
			              rpt.imgPathPass(path);
			    
					 }
					 
					 else
					 {
						  rpt.createTest("MayerBrown - People Search  - Verify Job Function -Check Box ", "Message :"+id.getValue()+"is NOT Displayed in PLP Relevance Drop Down");
			              rpt.Fail("Message :"+id.getValue()+" All options in Job function checkbox not  selected");
			              rpt.Category("MayerBrown - People Search");
			              String path = rpt.CaptureScreen(browser, "InvalidMessage");
			              rpt.imgPathFail(path);
			              

					 }
				 
			}
				 
			}
			
	public void MB_DbSearchwithoutInput() throws Exception
			{
				//click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
				if (findTheElement(
						 "xpath=html/body/div[1]/div/div[3]/ul/li[2]/a")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:People search tab is clicked");
						rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - People Search link is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:People search tab is NOT clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search link is NOT clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
				//click("xpath=//button[@id='db_search']");
				if (findTheElement(
						 "xpath=//button[@id='api_search']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Search is working fine with no inpu");
						rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - API Search button is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//button[@id='db_search']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:API Search button is not Clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - API Search button is NOT Clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
				Alert alert = browser.switchTo().alert();
				alert.accept();
				
				Thread.sleep(20000);
				
				
				String s=findTheElement("xpath=//div[@id='client_results_info']").getText();
				System.out.println(s);
				
				if(s.contains("entries"))
				{
					rpt.createTest("MayerBrown - People Search   - Without Input ", "Message : People search is working fine without input");
		              rpt.Pass("Message : People search is working fine without input");
		              rpt.Category("MayerBrown_People Search");
		              String path = rpt.CaptureScreen(browser, "ValidMessage");
		              rpt.imgPathPass(path);
				}
				
				else
				{
					rpt.createTest("MayerBrown - People Search   - Without Input ", "Message : People search is working fine without input");
		              rpt.Pass("Message : People search is working fine without input");
		              rpt.Category("MayerBrown_People Search");
		              String path = rpt.CaptureScreen(browser, "InvalidMessage");
		              rpt.imgPathPass(path);
				}
				
			}
			public void MB_DbSearchwithInput() throws Exception
			{
				//click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
				if (findTheElement(
						 "xpath=html/body/div[1]/div/div[3]/ul/li[2]/a")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page is displayed", "Message:People search tab is clicked");
						rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - People Search link is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:People search tab is NOT clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search link is NOT clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
				Thread.sleep(20000);
				//click("xpath=//input[@id='id_first_name']");
				
				if (findTheElement(
						 "xpath=//input[@id='id_first_name']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page  is displayed", "Message:People search First Name field is clicked");
						rpt.Pass("MAYER BROWN - People Search(DB)- Message:People search First Name field is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//input[@id='id_first_name']");
						

					} else {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page  is displayed", "Message:People search First Name field is NOT clicked");
						rpt.Fail("MAYER BROWN - People Search(DB)- Message:People search First Name field is NOT clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathFail(path);
						}
				
				
				//sendKeys("xpath=//input[@id='id_first_name']","Kanimozhi");
				
				if (findTheElement(
						 "xpath=//input[@id='id_first_name']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page  is displayed", "Message:People search First Name field is Entered");
						rpt.Pass("MAYER BROWN - People Search(DB)- Message:People search First Name field is Entered");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						sendKeys("xpath=//input[@id='id_first_name']","Kanimozhi");
						

					} else {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page  is displayed", "Message:People search First Name field is NOT Entered");
						rpt.Fail("MAYER BROWN - People Search(DB)- Message:People search First Name field is NOT Entered");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathFail(path);
						}
			/*	sendKeys("xpath=//input[@id='id_last_name']","");
				sendKeys("xpath=//input[@id='id_company_name']","");
				sendKeys("xpath=//textarea[@id='id_keywords']","");
				//sendKeys("xpath=//textarea[@id='id_duns_number']","");
				sendKeys("xpath=//input[@id='id_zipcode']","");*/
				//click("xpath=//button[@id='db_search']");
				if (findTheElement(
						 "xpath=//button[@id='db_search']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Search is working fine with no inpu");
						rpt.Pass("MAYER BROWN - People Search(API)- Click People Search link - API Search button is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//button[@id='db_search']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:API Search button is not Clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - API Search button is NOT Clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
				Thread.sleep(20000);
				
				String s=findTheElement("xpath=//div[@id='client_results_info']").getText();
				System.out.println(s);
				
				if(s.contains("entries"))
				{
					
						rpt.createTest("MayerBrown - People Search   - Without Input ", "Message : People search is working fine with input");
			              rpt.Pass("Message : People search is working fine with input");
			              rpt.Category("MayerBrown_People Search");
			              String path = rpt.CaptureScreen(browser, "ValidMessage");
			              rpt.imgPathPass(path);
					}
					
					else
					{
						rpt.createTest("MayerBrown - People Search   - With Input ", "Message : People search is working fine with input");
			              rpt.Pass("Message : People search is working fine with input");
			              rpt.Category("MayerBrown_People Search");
			              String path = rpt.CaptureScreen(browser, "InvalidMessage");
			              rpt.imgPathPass(path);
					}
			}
			
			public void DB_Filter_CommonCode() throws Exception
			{
				//click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
				if (findTheElement(
						 "xpath=html/body/div[1]/div/div[3]/ul/li[2]/a")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search-  People Search page is displayed", "Message:People search tab is clicked");
						rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link - People Search link is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:People search tab is NOT clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search link is NOT clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
				
				//sendKeys("xpath=//input[@id='id_first_name']","TEST");

				if (findTheElement(
						 "xpath=//input[@id='id_first_name']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page  is displayed", "Message:People search First Name field is Entered");
						rpt.Pass("MAYER BROWN - People Search(DB)- Message:People search First Name field is Entered");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						sendKeys("xpath=//input[@id='id_first_name']","TEST");
						

					} else {
						rpt.createTest("MAYER BROWN_People Search(API) - Verify People Search-  People Search page  is displayed", "Message:People search First Name field is NOT Entered");
						rpt.Fail("MAYER BROWN - People Search(DB)- Message:People search First Name field is NOT Entered");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathFail(path);
						}
				sendKeys("xpath=//input[@id='id_last_name']","");
				sendKeys("xpath=//input[@id='id_company_name']","");
				sendKeys("xpath=//textarea[@id='id_keywords']","");
				//sendKeys("xpath=//textarea[@id='id_duns_number']","");
				sendKeys("xpath=//input[@id='id_zipcode']","");
				
				//click("xpath=//button[@id='db_search']");
				if (findTheElement(
						 "xpath=//button[@id='db_search']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Search is working fine with no inpu");
						rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link - DB Search button is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//button[@id='db_search']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(DB)-   People Search page is NOT displayed", "Message:DB Search button is not Clicked");
						rpt.Fail("MAYER BROWN - People Search(DB)- Click People Search link - DB Search button is NOT Clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
			}
			
			public void Db_Filter_Country() throws Exception
			{
				DB_Filter_CommonCode();
				Thread.sleep(5000);
				//Drop down Selection
				Select drpCountry = new Select(findTheElement("id=countrynav"));
				drpCountry.selectByIndex(1);
				
				String selectedCountry=	drpCountry.getFirstSelectedOption().getText();
				//click("xpath=//button[@id='navreload']");
				if (findTheElement(
						 "xpath=//button[@id='navreload']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Search is GO button is Clicked");
						rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link - DB Search GO button is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//button[@id='navreload']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(DB)-   People Search page is NOT displayed", "Message:DB Search GO button is not Clicked");
						rpt.Fail("MAYER BROWN - People Search(DB)- Click People Search link - DB Search GO button is NOT Clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
		
				
				Thread.sleep(2000);
				String displayedCountry=findTheElement("xpath=.//*[@id='client_results']/tbody/tr[1]/td[10]").getText();
				
				if(selectedCountry.contains(displayedCountry))
				{ rpt.createTest("MayerBrown - People Search   - Verify Result page Filter - Country Filter ", "Message : Country Filter option is working fine");
	              rpt.Pass("Message : Country Filter option is working fine");
	              rpt.Category("MayerBrown_People Search");
	              String path = rpt.CaptureScreen(browser, "ValidMessage");
	              rpt.imgPathPass(path);
	    
			 }
			 
			 else
			 {
				  rpt.createTest("MayerBrown - People Search  - Verify Result page Filter - Country Filter ", "Message : Country Filter option is NOT working fine");
	              rpt.Fail("Message : Country Filter option is NOT working fine");
	              rpt.Category("MayerBrown - People Search");
	              String path = rpt.CaptureScreen(browser, "InvalidMessage");
	              rpt.imgPathFail(path);
			 }
				
}
			
			public void Db_Filter_StateProvince() throws Exception
			{
				DB_Filter_CommonCode();
				Thread.sleep(5000);
				//Drop down Selection
				Select drpState = new Select(findTheElement("id=statenav"));
				drpState.selectByIndex(1);
				
				String selectedState=	drpState.getFirstSelectedOption().getText();
				click("xpath=//button[@id='navreload']");
				Thread.sleep(2000);
				String displayedState=findTheElement("xpath=.//*[@id='client_results']/tbody/tr[1]/td[9]").getText();
				
				if(selectedState.contains(displayedState))
				{ rpt.createTest("MayerBrown - People Search   - Verify Result page Filter - StateProvince Filter ", "Message : StateProvince Filter option is working fine");
	              rpt.Pass("Message :StateProvince Filter option is working fine");
	              rpt.Category("MayerBrown_People Search");
	              String path = rpt.CaptureScreen(browser, "ValidMessage");
	              rpt.imgPathPass(path);
	    
			 }
			 
			 else
			 {
				  rpt.createTest("MayerBrown - People Search  - Verify Result page Filter - StateProvince Filter ", "Message : StateProvince Filter option is NOT working fine");
	              rpt.Fail("Message : StateProvince Filter option is NOT working fine");
	              rpt.Category("MayerBrown - People Search");
	              String path = rpt.CaptureScreen(browser, "InvalidMessage");
	              rpt.imgPathFail(path);
			 }
				
			}
			
			public void MB_FieldAvailability() throws Exception
			{
				
				//click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
				if (findTheElement(
						 "xpath=html/body/div[1]/div/div[3]/ul/li[2]/a")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search-  People Search page is displayed", "Message:People search tab is clicked");
						rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link - People Search link is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=html/body/div[1]/div/div[3]/ul/li[2]/a");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(API)-   People Search page is NOT displayed", "Message:People search tab is NOT clicked");
						rpt.Fail("MAYER BROWN - People Search(API)- Click People Search link - People Search link is NOT clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
			
				Map<String,String> ids= new HashMap<String,String>();
				 ids.put("FirstName", "id_first_name"); 
				 ids.put("LastName", "id_last_name");
				 ids.put("CompanyName", "id_company_name");
				 ids.put("Job Function", "id_job_function");
				 ids.put("User Keywords", "id_keywords");
				 ids.put("DUNS Number", "id_duns_number");
				 ids.put("Country", "id_country");
				 ids.put("State", "id_state");
				 ids.put("PostCode", "id_zipcode");
				 
				 
				                   
				 for(Map.Entry<String, String> id : ids.entrySet() )
				 {
					 if(findTheElement("xpath=//*[@id='"+id.getValue()+"']").isDisplayed())
					 {
						 rpt.createTest("MAYER BROWN - Field Availability", "Message :"+id.getKey()+" Displayed In People Search Page");
			              rpt.Pass("Message :"+id.getKey()+" Displayed In People Search Page");
			              rpt.Category("SVP_PLP");
			              String path = rpt.CaptureScreen(browser, "ValidMessage");
			              rpt.imgPathPass(path);
			
					 }
					 
					 else
					 {
						  rpt.createTest("MAYER BROWN - Field Availability", "Message :"+id.getKey()+" NOT Displayed In People Search Page");
			              rpt.Fail("Message :"+id.getKey()+" NOT Displayed In People Search Page");
			              rpt.Category("SVP_PLP");
			              String path = rpt.CaptureScreen(browser, "InvalidMessage");
			              rpt.imgPathFail(path);

					 }
				 }
			}
			
			/*public void Db_Filter_City() throws Exception
			{
				DB_Filter_CommonCode();
				Thread.sleep(5000);
				//Drop down Selection
				Select drpCity = new Select(findTheElement("id=citynav"));
				drpCity.selectByIndex(1);
				
				String selectedCity=	drpCity.getFirstSelectedOption().getText();
				click("xpath=//button[@id='navreload']");
				Thread.sleep(2000);
				String displayedCity=findTheElement("xpath=.//*[@id='client_results']/tbody/tr[1]/td[9]").getText();
				
				if(selectedCity.contains(displayedCity))
				{ rpt.createTest("MayerBrown - People Search   - Verify Result page Filter - StateProvince Filter ", "Message : StateProvince Filter option is working fine");
	              rpt.Pass("Message :StateProvince Filter option is working fine");
	              rpt.Category("MayerBrown_People Search");
	              String path = rpt.CaptureScreen(browser, "ValidMessage");
	              rpt.imgPathPass(path);
	    
			 }
			 
			 else
			 {
				  rpt.createTest("MayerBrown - People Search  - Verify Result page Filter - StateProvince Filter ", "Message : StateProvince Filter option is NOT working fine");
	              rpt.Fail("Message : StateProvince Filter option is NOT working fine");
	              rpt.Category("MayerBrown - People Search");
	              String path = rpt.CaptureScreen(browser, "InvalidMessage");
	              rpt.imgPathFail(path);
			 }
				
			}*/
			
			public void Db_Filter_Title() throws Exception
			{
				DB_Filter_CommonCode();
				Thread.sleep(5000);
				//Drop down Selection
				/*Select drpTitle = new Select(findTheElement("id=titlenav"));
				drpTitle.selectByIndex(1);*/
				
				click("xpath=.//*[@id='container-main']/div[1]/div[4]/div/button");
				click("xpath=.//*[@id='container-main']/div[1]/div[4]/div/ul/li[2]/a/label");
				
			String	selectedTitle=findTheElement("xpath=.//*[@id='container-main']/div[1]/div[4]/div/ul/li[2]/a/label").getText();
				
				//String selectedTitle=	drpTitle.getFirstSelectedOption().getText();
				//click("xpath=//button[@id='navreload']");
				if (findTheElement(
						 "xpath=//button[@id='navreload']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Search is GO button is Clicked");
						rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link - DB Search GO button is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//button[@id='navreload']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(DB)-   People Search page is NOT displayed", "Message:DB Search GO button is not Clicked");
						rpt.Fail("MAYER BROWN - People Search(DB)- Click People Search link - DB Search GO button is NOT Clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				Thread.sleep(2000);
				String displayedTitle=findTheElement("xpath=.//*[@id='client_results']/tbody/tr[1]/td[7]").getText();
				
				if(selectedTitle.contains(displayedTitle))
				{ rpt.createTest("MayerBrown - People Search   - Verify Result page Filter - StateProvince Filter ", "Message : StateProvince Filter option is working fine");
	              rpt.Pass("Message :StateProvince Filter option is working fine");
	              rpt.Category("MayerBrown_People Search");
	              String path = rpt.CaptureScreen(browser, "ValidMessage");
	              rpt.imgPathPass(path);
	    
			 }
			 
			 else
			 {
				  rpt.createTest("MayerBrown - People Search  - Verify Result page Filter - StateProvince Filter ", "Message : StateProvince Filter option is NOT working fine");
	              rpt.Fail("Message : StateProvince Filter option is NOT working fine");
	              rpt.Category("MayerBrown - People Search");
	              String path = rpt.CaptureScreen(browser, "InvalidMessage");
	              rpt.imgPathFail(path);
			 }
				
			}
			
			public void DB_CompanyDetails() throws Exception
			{
				DB_Filter_CommonCode();
				Thread.sleep(5000);
				String TitleofClickedLink=findTheElement("xpath=//a[@href='/company/detail/220830513?fromsearch=null']").getText();
				
				//click("xpath=//a[@href='/company/detail/6998255?fromsearch=null']");
				if (findTheElement(
						 "xpath=//a[@href='/company/detail/220830513?fromsearch=null']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Search is Title button is Clicked");
						rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link - DB Search Title button is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//a[@href='/company/detail/220830513?fromsearch=null']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(DB)-   People Search page is NOT displayed", "Message:DB Search Title button is not Clicked");
						rpt.Fail("MAYER BROWN - People Search(DB)- Click People Search link - DB Search Title button is NOT Clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				Thread.sleep(5000);
				
				String titleDisplayedInLeftPanel=findTheElement("xpath=.//*[@id='container-main']/div/div[1]/div[1]/h4").getText();
				
				if(TitleofClickedLink.equals(titleDisplayedInLeftPanel))
				{
									
						rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search (DB Search)-  Company Details page is displayed", "Message: DB Company Details page is displaying with Title");
						rpt.Pass("MAYER BROWN - People Search(DB)-  -DB Company Details page is displaying with Title");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						
					}
					else
					{
						rpt.createTest("MAYER BROWN - Verify People Search(DB)-   People Search page is NOT displayed", "Message:DB Company Details page is NOT displaying with Title");
						rpt.Fail("MAYER BROWN - People Search(DB)- Click People Search link -DB Company Details page is NOT displaying with Title");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
					}
				String titleDisplayeInLeftPanel=findTheElement("xpath=.//*[@id='container-main']/div/div[1]/div[4]/h5").getText();
				
				if(titleDisplayeInLeftPanel.contains("Financial History"))
				{
					rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Company Details page is displaying with Finance History Panel");
					rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link -DB Company Details page is displaying with Finance History Panel");
					rpt.Category("MAYER BROWN_PEOPLE SEARCH");
					String path = rpt.CaptureScreen(browser, "ValidMessage");
					rpt.imgPathPass(path);
					
				}
				else
				{
					rpt.createTest("MAYER BROWN - Verify People Search(DB)-   People Search page is NOT displayed", "Message:DB Company Details page is NOT displaying with Finance History Panel");
					rpt.Fail("MAYER BROWN - People Search(DB)- Click People Search link -DB Company Details page is NOT displaying with Finance History Panel");
					rpt.Category("MAYER BROWN_PEOPLE SEARCH");
					String path = rpt.CaptureScreen(browser, "InvalidMessage");
					rpt.imgPathFail(path);
				}
			
				
				DownloadFunctionality_MB dwnl = new DownloadFunctionality_MB();
				dwnl.verifyPrintButtonInCompanyDetailPage();
				dwnl.verifyAlternativeAndMinorityButtonInCompanyDetailPage();
				dwnl.verifyDownloadButtonInCompanyDetailPage();
				
				browser.navigate().back();
				click("xpath=html/body/div[1]/div/div[3]/ul/li[1]/a");
				Logout lg=new Logout();
				lg.logout();
				
			}
			
			public void MB_FinancialHistory() throws Exception
			{
				DB_Filter_CommonCode();
				
				
				//click("xpath=//a[@href='/company/detail/6998255?fromsearch=null']");
				if (findTheElement(
						 "xpath=//a[@href='/company/detail/6998255?fromsearch=null']")
							.isDisplayed()) {
						rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Company Details link is clicked");
						rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link -DB Company Details link is clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "ValidMessage");
						rpt.imgPathPass(path);
						click("xpath=//a[@href='/company/detail/6998255?fromsearch=null']");
						

					} else {
						rpt.createTest("MAYER BROWN - Verify People Search(DB)-   People Search page is NOT displayed", "Message:DB Company Details link is NOTclicked");
						rpt.Fail("MAYER BROWN - People Search(DB)- Click People Search link - DB Company Details link is NOT clicked");
						rpt.Category("MAYER BROWN_PEOPLE SEARCH");
						String path = rpt.CaptureScreen(browser, "InvalidMessage");
						rpt.imgPathFail(path);
						
					}
				
				String titleDisplayedInLeftPanel=findTheElement("xpath=.//*[@id='container-main']/div/div[1]/div[4]/h5").getText();
				
				if(titleDisplayedInLeftPanel.contains("Financial History"))
				{
					rpt.createTest("MAYER BROWN_People Search(DB) - Verify People Search (DB Search)-  People Search page is displayed", "Message: DB Company Details page is displaying with Finance History Panel");
					rpt.Pass("MAYER BROWN - People Search(DB)- Click People Search link -DB Company Details page is displaying with Finance History Panel");
					rpt.Category("MAYER BROWN_PEOPLE SEARCH");
					String path = rpt.CaptureScreen(browser, "ValidMessage");
					rpt.imgPathPass(path);
					
				}
				else
				{
					rpt.createTest("MAYER BROWN - Verify People Search(DB)-   People Search page is NOT displayed", "Message:DB Company Details page is NOT displaying with Finance History Panel");
					rpt.Fail("MAYER BROWN - People Search(DB)- Click People Search link -DB Company Details page is NOT displaying with Finance History Panel");
					rpt.Category("MAYER BROWN_PEOPLE SEARCH");
					String path = rpt.CaptureScreen(browser, "InvalidMessage");
					rpt.imgPathFail(path);
				}
				
			
			}
			
			
			
			
}
