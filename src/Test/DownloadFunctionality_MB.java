package Test;



import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Set;

import org.openqa.selenium.Keys;

	public class DownloadFunctionality_MB extends Browser {
		
		Report rpt = new Report();
		
		CommonUtils email = new CommonUtils();
			
			public void executeAll() throws Exception
			{
				Thread.sleep(2000);
				
				navigateToCompanyNamePage();
				verifyCompanyNamePageDownloadButton();
				verifyClickCompanyName();
				verifyPrintButtonInCompanyDetailPage();
				verifyAlternativeAndMinorityButtonInCompanyDetailPage();
				verifyDownloadButtonInCompanyDetailPage();
//				verifyCompanyNamePeopleSeacrhPrintButton();
				

			}
			
			public void navigateToCompanyNamePage() throws Exception
			{
				//Click Company Name textbox
				if(findTheElement("xpath=//a[@href='/company']").isDisplayed())
				{
					click("xpath=//a[@href='/company']");
					 rpt.createTest("MayerBrown", "Click Company Name textbox");
			         rpt.Pass("Company Name textbox is clicked");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Click Company Name textbox");
			         rpt.Fail("Company Name textbox is NOT clicked");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				
				//Enter Company Name
				if(findTheElement("xpath=//*[@id=\"id_company_name\"]").isDisplayed())
				{
					sendKeys("xpath=//*[@id=\"id_company_name\"]","IBM");
					 rpt.createTest("MayerBrown", "Enter Company Name");
			         rpt.Pass("Company Name is entered");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Enter Company Name");
			         rpt.Fail("Company Name is NOT entered");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				
				//Click Advanced Search button
				if(findTheElement("id=id_adv_search_type").isDisplayed())
				{
					click("id=id_adv_search_type");
					 rpt.createTest("MayerBrown", "Click Advanced Search button");
			         rpt.Pass("Advanced Search button is clicked");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Click Advanced Search button");
			         rpt.Fail("Advanced Search button is NOT clicked");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
			}
			public void verifyCompanyNamePageDownloadButton() throws Exception{
				//Click Download Button in Company Name page
				if(findTheElement("xpath=//*[@id='download']").isDisplayed())
				{
					click("xpath=//*[@id='download']");
					Robot r = new Robot();
					r.keyPress(KeyEvent.VK_TAB);
					r.keyRelease(KeyEvent.VK_TAB);
					r.keyPress(KeyEvent.VK_TAB);
					r.keyRelease(KeyEvent.VK_TAB);
					r.keyPress(KeyEvent.VK_TAB);
					r.keyRelease(KeyEvent.VK_TAB);

					Thread.sleep(2000);

					r.keyPress(KeyEvent.VK_ENTER);
					r.keyRelease(KeyEvent.VK_ENTER);
					r.keyPress(KeyEvent.VK_ENTER);
					r.keyRelease(KeyEvent.VK_ENTER);
					
					 rpt.createTest("MayerBrown", "Click Download Button in Company Seacrh in Company Name page");
			         rpt.Pass("Download Button in Company Name page is clicked and download");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Click Download Button in Company Seacrh in Company Name page");
			         rpt.Fail("Download Button in Company Name page is not clicked");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				
			}
				
				public void verifyClickCompanyName() throws Exception{
				//Click Company Name
				if(findTheElement("xpath=//*[@id='company_results']/tbody/tr[1]/td[3]/a").isDisplayed())
				{
					click("xpath=//*[@id='company_results']/tbody/tr[1]/td[3]/a");
					 rpt.createTest("MayerBrown", "Click Company Name");
			         rpt.Pass("Company Name is clicked");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Click Company Name");
			         rpt.Fail("Company Name is NOT clicked");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				}
				
			
	public void verifyPrintButtonInCompanyDetailPage() throws Exception{
		
		click("xpath=//*[@id='container-main']/div/div[1]/div[1]/a[1]/i");
		Thread.sleep(13000);
		String parentWindow = browser.getWindowHandle();
		Set<String> handles =  browser.getWindowHandles();
		for(String windowHandle  : handles)
		{
		    if(!windowHandle.equals(parentWindow))
		   {
		    	browser.switchTo().window(windowHandle);
		     String childWindowTitle = getTitle();
		     System.out.print(childWindowTitle);
		     browser.close(); 
		     browser.switchTo().window(parentWindow); 
		     String DUNS_Number = getText("xpath=//*[@id='container-main']/div/div[1]/div[1]/dl[1]/dd");
		     if(DUNS_Number.contains(childWindowTitle))
				{
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Pass("DUNS Number in Company Detail page and DUNS Number in Print Screen are equal - DUNS_Number : "+childWindowTitle+" ");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Fail("DUNS Number in Company Detail page and DUNS Number in Print Screen are NOT equal- DUNS_Number : "+childWindowTitle+"");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
		    }
		}
		
		click("xpath=//*[@id='treedetails']/a[1]/i");
		Thread.sleep(10000);
		String parentWindow1 = browser.getWindowHandle();
		Set<String> handles1 =  browser.getWindowHandles();
		for(String windowHandle  : handles1)
		{
		    if(!windowHandle.equals(parentWindow1))
		   {
		    	browser.switchTo().window(windowHandle);
		     String childWindowTitle = getTitle();
		     System.out.print(childWindowTitle);
		     browser.close(); 
		     browser.switchTo().window(parentWindow1); 
		     String DUNS_Number = getText("xpath=//*[@id='treedetails']/dl[1]/dd/a");
		     if(DUNS_Number.contains(childWindowTitle))
				{
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Pass("DUNS Number in Company Detail page and DUNS Number in Print Screen are equal- DUNS_Number : "+childWindowTitle+"");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Fail("DUNS Number in Company Detail page and DUNS Number in Print Screen are NOT equal- DUNS_Number : "+childWindowTitle+"");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
		    }
		}
		
		click("xpath=//*[@id='treedetails']/a[2]/i");
		Thread.sleep(10000);
		String parentWindow2 = browser.getWindowHandle();
		Set<String> handles2 =  browser.getWindowHandles();
		for(String windowHandle  : handles2)
		{
		    if(!windowHandle.equals(parentWindow2))
		   {
		    	browser.switchTo().window(windowHandle);
		     String childWindowTitle = getTitle();
		     System.out.print(childWindowTitle);
		     browser.close(); 
		     browser.switchTo().window(parentWindow2); 
		     String DUNS_Number = getText("xpath=//*[@id='treedetails']/dl[2]/dd/a");
		     if(DUNS_Number.contains(childWindowTitle))
				{
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Pass("DUNS Number in Company Detail page and DUNS Number in Print Screen are equal- DUNS_Number : "+childWindowTitle+"");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Fail("DUNS Number in Company Detail page and DUNS Number in Print Screen are NOT equal- DUNS_Number : "+childWindowTitle+"");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
		    }
		}
		
		click("xpath=//*[@id='treedetails']/a[3]/i");
		Thread.sleep(10000);
		String parentWindow3 = browser.getWindowHandle();
		Set<String> handles3 =  browser.getWindowHandles();
		for(String windowHandle  : handles3)
		{
		    if(!windowHandle.contains(parentWindow3))
		   {
		    	browser.switchTo().window(windowHandle);
		     String childWindowTitle = getTitle();
		     System.out.print(childWindowTitle);
		     browser.close(); 
		     browser.switchTo().window(parentWindow3); 
		     String DUNS_Number = getText("xpath=//*[@id='treedetails']/dl[3]/dd/a");
		     if(DUNS_Number.contains(childWindowTitle))
				{
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Pass("DUNS Number in Company Detail page and DUNS Number in Print Screen are equal- DUNS_Number : "+childWindowTitle+"");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Fail("DUNS Number in Company Detail page and DUNS Number in Print Screen are NOT equal- DUNS_Number : "+childWindowTitle+"");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
		    }
		}
		
		click("xpath=//*[@id='tab_familytree']/div/div[1]/button[8]");
		Thread.sleep(10000);
		String parentWindow4 = browser.getWindowHandle();
		Set<String> handles4 =  browser.getWindowHandles();
		for(String windowHandle  : handles4)
		{
		    if(!windowHandle.equals(parentWindow4))
		   {
		    	browser.switchTo().window(windowHandle);
		     String childWindowTitle = getTitle();
		     System.out.print(childWindowTitle);
		     browser.close(); 
		     browser.switchTo().window(parentWindow4); 
		     String DUNS_Number = getText("xpath=//*[@id='container-main']/div/div[1]/div[1]/dl[1]/dd");
		     if(childWindowTitle.contains(DUNS_Number))
				{
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Pass("DUNS Number in Company Detail page and DUNS Number in Print Screen are equal- DUNS_Number : "+childWindowTitle+"");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Verify DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Fail("DUNS Number in Company Detail page and DUNS Number in Print Screen are NOT equal- DUNS_Number : "+childWindowTitle+"");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
		    }
		}
		
		click("xpath=//*[@id='tab_familytree']/div/div[1]/button[7]");
		Thread.sleep(13000);
		String parentWindow5 = browser.getWindowHandle();
		Set<String> handles5 =  browser.getWindowHandles();
		for(String windowHandle  : handles5)
		{
		    if(!windowHandle.equals(parentWindow5))
		   {
		    	browser.switchTo().window(windowHandle);
		     String childWindowTitle = getTitle();
		     System.out.print(childWindowTitle);
		     browser.close(); 
		     browser.switchTo().window(parentWindow5); 
		     String DUNS_Number = getText("xpath=//*[@id='container-main']/div/div[1]/div[1]/dl[1]/dd");
		     Thread.sleep(2000);
		     if(DUNS_Number.contains(childWindowTitle))
				{
					 rpt.createTest("MayerBrown", "Verify Consolidated Print is clicked and DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Pass("Consolidated Print is clicked and DUNS Number in Company Detail page and DUNS Number in Print Screen are equal");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "ValidMessage");
			         rpt.imgPathPass(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
				 else
				 {
					 rpt.createTest("MayerBrown", "Verify Consolidated Print is clicked and DUNS Number in Company Detail page and DUNS Number in Print Screen");
			         rpt.Fail("Consolidated Print is clicked and DUNS Number in Company Detail page and DUNS Number in Print Screen are NOT equal");
			         rpt.Category("MayerBrown");
			         String path = rpt.CaptureScreen(browser, "InvalidMessage");
			         rpt.imgPathFail(path);
			         rpt.chkBugs(CommonUtils.getUserName());
				 }
		    }
		}
		
		
	}
	public void verifyAlternativeAndMinorityButtonInCompanyDetailPage() throws Exception
	{
		//Click Alternative Button in Company Detail page
		if(findTheElement("xpath=//*[@id='tab_familytree']/div/div[1]/button[5]").isDisplayed())
		{
			click("xpath=//*[@id='tab_familytree']/div/div[1]/button[5]");
			 rpt.createTest("MayerBrown", "Click Alternative Button in Company Detail page");
	         rpt.Pass("Alternative Button is clicked in Company Detail page");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "ValidMessage");
	         rpt.imgPathPass(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		 }
		 else
		 {
			 rpt.createTest("MayerBrown", "Click Alternative Button in Company Detail page");
	         rpt.Fail("Alternative Button is NOT clicked in Company Detail page");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		 }
		
		
		//Click Majority Button
		if(findTheElement("xpath=//*[@id='tab_familytree']/div/div[1]/button[6]").isDisplayed())
		{
			click("xpath=//*[@id='tab_familytree']/div/div[1]/button[6]");
			 rpt.createTest("MayerBrown", "Click Minority Button in Company Detail page");
	         rpt.Pass("Minority Button is clicked in Company Detail page");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "ValidMessage");
	         rpt.imgPathPass(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		 }
		 else
		 {
			 rpt.createTest("MayerBrown", "Click Minority Button in Company Detail page");
	         rpt.Fail("Minority Button is NOT clicked in Company Detail page");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		 }
		
	}
	
	public void verifyDownloadButtonInCompanyDetailPage() throws Exception{
		
		if(findTheElement("xpath=//*[@id='container-main']/div/div[1]/div[1]/a[2]/i").isDisplayed()){
		click("xpath=//*[@id='container-main']/div/div[1]/div[1]/a[2]/i");
		Thread.sleep(1000);
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);

		Thread.sleep(2000);

		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

//		r.keyPress(KeyEvent.VK_ALT);
//		r.keyPress(KeyEvent.VK_F4);
//		
//		r.keyRelease(KeyEvent.VK_F4);
//		r.keyRelease(KeyEvent.VK_ALT);
		
		rpt.createTest("MayerBrown", "Verify Download Button in Company Detail page is displayed and clicked");
        rpt.Pass("Download Button in Company Detail page is displayed and clicked");
        rpt.Category("MayerBrown");
        String path = rpt.CaptureScreen(browser, "ValidMessage");
        rpt.imgPathPass(path);
        rpt.chkBugs(CommonUtils.getUserName());
		}
		else{
			rpt.createTest("MayerBrown", "Verify Download Button in Company Detail page is NOT displayed");
	         rpt.Fail("Download Button in Company Detail page is NOT displayed");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		}
		
		Thread.sleep(2000);
		
		if(findTheElement("xpath=//*[@id='dLabel']").isDisplayed())
		{
		click("xpath=//*[@id='dLabel']");
		rpt.createTest("MayerBrown", "Verify Download dropdown in Company Detail page is displayed and clicked");
        rpt.Pass("Download dropdown in Company Detail page is displayed and clicked");
        rpt.Category("MayerBrown");
        String path = rpt.CaptureScreen(browser, "ValidMessage");
        rpt.imgPathPass(path);
        rpt.chkBugs(CommonUtils.getUserName());
		}
		else{
			rpt.createTest("MayerBrown", "Verify Download dropdown in Company Detail page is NOT displayed");
	         rpt.Fail("Download dropdown in Company Detail page is NOT displayed");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		}
		
		
		
		if(findTheElement("xpath=//*[@id='tab_familytree']/div/div[1]/div[2]/ul/li[1]/a").isDisplayed())
		{
			click("xpath=//*[@id='tab_familytree']/div/div[1]/div[2]/ul/li[1]/a");
			
			Robot r= new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);

			Thread.sleep(2000);

			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			
		rpt.createTest("MayerBrown", "Verify Hierarchy Export is clicked");
        rpt.Pass("Hierarchy Export is clicked");
        rpt.Category("MayerBrown");
        String path = rpt.CaptureScreen(browser, "ValidMessage");
        rpt.imgPathPass(path);
        rpt.chkBugs(CommonUtils.getUserName());
		}
		else{
			rpt.createTest("MayerBrown", "Verify Hierarchy Export is NOT clicked");
	         rpt.Fail("Hierarchy Export is NOT clicked");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		}
		
		if(findTheElement("xpath=//*[@id='dLabel']").isDisplayed())
		{
			click("xpath=//*[@id='dLabel']");
			click("xpath=//*[@id='tab_familytree']/div/div[1]/div[2]/ul/li[2]/a");
			
			Robot r= new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);

			Thread.sleep(2000);

			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			
		rpt.createTest("MayerBrown", "Verify Standard Export is clicked");
        rpt.Pass("Standard Export is clicked");
        rpt.Category("MayerBrown");
        String path = rpt.CaptureScreen(browser, "ValidMessage");
        rpt.imgPathPass(path);
        rpt.chkBugs(CommonUtils.getUserName());
		}
		else{
			rpt.createTest("MayerBrown", "Verify Standard Export is NOT clicked");
	         rpt.Fail("Standard Export is NOT clicked");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		}
		
		if(findTheElement("xpath=//*[@id='dLabel']").isDisplayed())
		{
			click("xpath=//*[@id='dLabel']");
			click("xpath=//*[@id='tab_familytree']/div/div[1]/div[2]/ul/li[3]/a");
			
			Robot r= new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);

			Thread.sleep(2000);

			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			
		rpt.createTest("MayerBrown", "Verify LegalKey Export is clicked");
        rpt.Pass("LegalKey Export is clicked");
        rpt.Category("MayerBrown");
        String path = rpt.CaptureScreen(browser, "ValidMessage");
        rpt.imgPathPass(path);
        rpt.chkBugs(CommonUtils.getUserName());
		}
		else{
			rpt.createTest("MayerBrown", "Verify LegalKey Export is NOT clicked");
	         rpt.Fail("LegalKey Export is NOT clicked");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		}
		
	}
	
	
	public void verifyCompanyNamePeopleSeacrhPrintButton() throws Exception{
		//Click Download Button in Company Name page
		
		click("xpath=//*[@id='tab_familytree']/div/div[1]/div[2]/ul/li[3]/a");
		click("xpath=//*[@id='api_search']");
		if(findTheElement("xpath=//*[@id='print-peoplesearch']").isDisplayed())
		{
			click("xpath=//*[@id='print-peoplesearch']");
			Robot r= new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
					
			
						
			
						
			
						
			
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			
			 rpt.createTest("MayerBrown", "Click Print Button in People Seacrh in Company Name page");
	         rpt.Pass("Download Button in Company Name page is clicked and download");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "ValidMessage");
	         rpt.imgPathPass(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		 }
		 else
		 {
			 rpt.createTest("MayerBrown", "Click Print Button in People Seacrh in Company Name page");
	         rpt.Fail("Download Button in Company Name page is not clicked");
	         rpt.Category("MayerBrown");
	         String path = rpt.CaptureScreen(browser, "InvalidMessage");
	         rpt.imgPathFail(path);
	         rpt.chkBugs(CommonUtils.getUserName());
		 }
		
	}
	}

