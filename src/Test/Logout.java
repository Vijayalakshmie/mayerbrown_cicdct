package Test;

public class Logout extends Browser {

	Report rpt = new Report();
	public void logout() throws Exception {
		//Logout from Application 
		click("xpath=html/body/div[1]/div/div[3]/ul/li[5]/a");
		if (findTheElement("xpath=html/body/div[1]/div/div[3]/ul/li[5]/a").isDisplayed()) {
			rpt.createTest("MB- Logout","Logout From The application-Click Logout button");
			rpt.Pass("Logout - Logout button is clicked ");
			rpt.Category("MB - Logout");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			click("xpath=html/body/div[1]/div/div[3]/ul/li[5]/a");

		} else {
			rpt.createTest("MB- Logout - Check Logout Functionality", "Logout Button is NOT Displayed");
			rpt.Fail("Logout_Functionality - Logout Button is NOT Clicked ");
			rpt.Category("MB- Logout -Check Logout Functionality");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
		}
		
		
	
	
	
}
}