package Test;

import java.awt.Checkbox;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.support.ui.Select;

public class CompanySearch extends Browser {
	Report rpt = new Report();

	public void executeAll() throws Exception {
		pageLoadCheck();
		adpativeSearch();
		standardSearch();
		recentSearches();
		searchResultPages();
		filtersInResultPage_databaseSearch();
		filtersINResultpage_apiSearch();
		downloadFunctions();
	}

	public void pageLoadCheck() throws Exception {
		click("xpath=//a[@href='/company']");
		Map<String, String> forms = new HashMap<String, String>();
		forms.put("Adaptive Search", "//*[@id=\"container-main\"]/div[2]/div[1]/div/div/div[1]/div/form");
		forms.put("Standard Search", "//*[@id=\"container-main\"]/div[2]/div[1]/div/div/div[2]/div/form");
		forms.put("Recent Searches", "//*[@class=\"nav nav-list\"]");
		for (Map.Entry<String, String> form : forms.entrySet()) {
			if (findTheElement("xpath=" + form.getValue() + "").isDisplayed()) {
				rpt.createTest("MB- Company Search Page -Check search forms & Recent Searches", "Message : Element -"
						+ form.getKey() + " Displayed - For Checking search forms & Recent Searches");
				rpt.Pass("Message :Element -" + form.getKey()
						+ " Displayed - For Checking search forms & Recent Searches");
				rpt.Category("MB- Company Search Page -Check search forms & Recent Searches");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				rpt.chkBugs(CommonUtils.getUserName());

			} else {
				rpt.createTest("MB- Company Search Page -Check search forms & Recent Searches", "Message : Element - "
						+ form.getKey() + "is NOT Displayed - For Checking search forms & Recent Searches");
				rpt.Pass("Message : Element - " + form.getKey()
						+ " NOT Displayed - For Checking search forms & Recent Searches");
				rpt.Category("MB- Company Search Page -Check search forms & Recent Searches");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				rpt.chkBugs(CommonUtils.getUserName());

			}
		}
	}

	public void adpativeSearch() throws Exception {
		// Without Input
		Map<String, String> WithoutInputs = new HashMap<String, String>();
		WithoutInputs.put("Advance Search", "id_adv_search_type");
		WithoutInputs.put("API Search", "id_api_search_type");
		for (Entry<String, String> withoutInput : WithoutInputs.entrySet()) {
			if (findTheElement("xpath=//*[@id=\"" + withoutInput.getValue() + "\"]").isDisplayed()) {
				click("xpath=//*[@id=\"" + withoutInput.getValue() + "\"]");
				waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
				waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");
				rpt.createTest("MB- Company Search Page - Check search functions without_inputs",
						"Message : Button - " + withoutInput.getKey()
								+ " without_inputs : Displayed - For Check search functions without_inputs");
				rpt.Pass("Message : Button - " + withoutInput.getKey()
						+ "without_inputs : Displayed - For Check search functions without_inputs");
				rpt.Category("MB- Company Search Page - Check search funtions without_inputs");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				rpt.chkBugs(CommonUtils.getUserName());
				waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
				waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");

			} else {
				rpt.createTest("MB- Company Search Page - Check search functions without_inputs",
						"Message :" + withoutInput.getKey()
								+ "without_inputs : is NOT Displayed - For Check search functions without_inputs");
				rpt.Pass("Message :" + withoutInput.getKey()
						+ "without_inputs : NOT Displayed - For Check search functions without_inputs");
				rpt.Category("MB- Company Search Page - Check search functions without_inputs");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				rpt.chkBugs(CommonUtils.getUserName());
			}
			browser.navigate().back();
		}

		// With Input
		sendKeys("xpath=//*[@id=\"id_company_name\"]", "IBM");// Search Box
		if (findTheElement("xpath=//*[@id=\"id_company_name\"]").isDisplayed()) {
			rpt.createTest("MB- Company Search Page -Check Search Box_AdaptiveSearch",
					"Search Box is Displayed - For Check Search Box_AdaptiveSearch");
			rpt.Pass("Search Box Displayed - For Check Search Box_AdaptiveSearch");
			rpt.Category("MB - Company Search Page -Check Search Box_AdaptiveSearch");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());
		} else {
			rpt.createTest("MB- Company Search Page - Check Search Box_AdaptiveSearch",
					"Search Box is NOT Displayed - For Check Search Box_AdaptiveSearch");
			rpt.Fail("Search Box is NOT Displayed - For Check Search Box_AdaptiveSearch");
			rpt.Category("MB- Company Search Page -Check Search Box_AdaptiveSearch");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		click("xpath=//*[@id=\"container-main\"]/div[1]/div/h3");
		Map<String, String[]> selects = new HashMap<String, String[]>();
		selects.put("Search Type", new String[] { "id_search_type", "Starts With" });
		selects.put("Company Type", new String[] { "id_company_type", "Current Name" });
		selects.put("Match Type", new String[] { "id_match_type", "Company Name Search" });
		for (Entry<String, String[]> select : selects.entrySet()) {
			if (findTheElement("xpath=//*[@id=\"" + select.getValue()[0] + "\"]").isDisplayed()) {
				rpt.createTest("MB- Comapny Search Page - Check Form Elements",
						"Message :" + select.getKey() + " Displayed - For Check Form Elements");
				rpt.Pass("Message :" + select.getKey() + " Displayed - For Check Form Elements");
				rpt.Category("MB- Comapny Search Page - Check Form Elements");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				rpt.chkBugs(CommonUtils.getUserName());
				select("xpath=//*[@id=\"" + select.getValue()[0] + "\"]", select.getValue()[1]);

			} else {
				rpt.createTest("MB- Comapny Search Page - Check Form Elements",
						"Message :" + select.getKey() + "is NOT Displayed - For Check Form Elements");
				rpt.Pass("Message :" + select.getKey() + " NOT Displayed - For Check Form Elements");
				rpt.Category("MB- Comapny Search Page - Check Form Elements");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				rpt.chkBugs(CommonUtils.getUserName());
			}
		}
		click("xpath=//*[@id=\"id_exclude_out_of_business\"]");
		if (!findTheElement("xpath=//*[@id=\"id_adv_search_type\"]").isEnabled()) {
			rpt.createTest("MB- Company Search Page -Check Advance_search_button validation",
					"Button validation is Working fine - For Check Advance_search_button validation");
			rpt.Pass("Button validation is Working fine - For Check Advance_search_button validation");
			rpt.Category("MB - Company Search Page -Check Advance_search_button validation");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());
		} else {
			rpt.createTest("MB- Company Search Page - Check Advance_search_button validation",
					"Button validation is Working fine - For Check Advance_search_button validation");
			rpt.Fail("Button validation is Working fine - For Check Advance_search_button validation");
			rpt.Category("MB- Company Search Page -Check Advance_search_button validation");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}

		if (!findTheElement("xpath=//*[@id=\"id_exclude_out_of_business\"]").isSelected()) {
			rpt.createTest("MB- Company Search Page -Check Check_Box-AdaptiveSearch",
					"Check box is not checked, So being enabled now - For Check Check_Box-AdaptiveSearch");
			rpt.Pass("Check box is not checked, So being enabled now - For Check Check_Box-AdaptiveSearch");
			rpt.Category("MB - Company Search Page -Check Check_Box-AdaptiveSearch");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());
			click("xpath=//*[@id=\"id_exclude_out_of_business\"]");
		} else {
			rpt.createTest("MB- Company Search Page - Check Check_Box-AdaptiveSearch",
					"Check box is already checked, So not being enabled now - For Check Check_Box-AdaptiveSearch");
			rpt.Fail("Check box is already checked, So not being enabled now - For Check Check_Box-AdaptiveSearch");
			rpt.Category("MB- Company Search Page -Check Check_Box-AdaptiveSearch");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		Map<String, String> buttons = new HashMap<String, String>();
		buttons.put("Advance Search", "id_adv_search_type");
		buttons.put("API Search", "id_api_search_type");
		for (Entry<String, String> button : buttons.entrySet()) {
			if (findTheElement("xpath=//*[@id=\"" + button.getValue() + "\"]").isDisplayed()) {
				click("xpath=//*[@id=\"" + button.getValue() + "\"]");
				waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
				waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");
				rpt.createTest("MB- Company Search Page - Check search functions",
						"Message : Button - " + button.getKey() + " Displayed - For Check search functions");
				rpt.Pass("Message : Button - " + button.getKey() + " Displayed - For Check search functions");
				rpt.Category("MB- Company Search Page - Check search funtions");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				rpt.chkBugs(CommonUtils.getUserName());
				waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
				waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");

			} else {
				rpt.createTest("MB- Company Search Page - Check search functions",
						"Message :" + button.getKey() + "is NOT Displayed - For Check search functions");
				rpt.Pass("Message :" + button.getKey() + " NOT Displayed - For Check search functions");
				rpt.Category("MB- Company Search Page - Check search functions");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				rpt.chkBugs(CommonUtils.getUserName());
			}
			browser.navigate().back();
		}
	}

	public void standardSearch() throws Exception {
		// Without Input
		if (findTheElement("xpath=//*[@id=\"container-main\"]/div[2]/div[1]/div/div/div[2]/div/form/div[4]/button")
				.isDisplayed()) {
			click("xpath=//*[@id=\"container-main\"]/div[2]/div[1]/div/div/div[2]/div/form/div[4]/button");
			waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
			waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");
			rpt.createTest("MB- Company Search Page -Check Standard Search without inputs",
					"Standard Search without inputs is Working - For Check Standard Search without inputs");
			rpt.Pass("Standard Search without inputs is Working - For Check Standard Search without inputs");
			rpt.Category("MB - Company Search Page -Check Standard Search without inputs");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());
		} else {
			rpt.createTest("MB- Company Search Page - Check Standard Search without inputs",
					"Standard Search without inputs is not Working - For Check Standard Search without inputs");
			rpt.Fail("Standard Search without inputs is not Working - For Check Standard Search without inputs");
			rpt.Category("MB- Company Search Page -Check Standard Search without inputs");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		browser.navigate().back();
		// With Input
		Map<String, String[]> options = new HashMap<String, String[]>();
		options.put("Duns Number", new String[] { "id_duns_numbers", "480008192" });
		options.put("Industry Code", new String[] { "id_industry_code", "27419903" });
		options.put("Street Address", new String[] { "id_street_address", "Vulkanstrasse" });
		for (Entry<String, String[]> option : options.entrySet()) {
			if (findTheElement("xpath=//*[@id=\"" + option.getValue()[0] + "\"]").isDisplayed()) {
				click("xpath=//*[@id=\"" + option.getValue()[0] + "\"]");
				sendKeys("xpath=//*[@id=\"" + option.getValue()[0] + "\"]", option.getValue()[1]);
				click("xpath=//*[@id=\"container-main\"]/div[2]/div[1]/div/div/div[2]/div/form/div[4]/button");
				waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
				waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");
				rpt.createTest("MB- Company Search Page - Check search functions",
						"Message : Approach - " + option.getKey() + " Displayed - For checking Standard Search ");
				rpt.Pass("Message : Approach - " + option.getKey() + " Displayed - For checking search functions");
				rpt.Category("MB- Company Search Page - Check search funtions");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				rpt.chkBugs(CommonUtils.getUserName());

			} else {
				rpt.createTest("MB- Company Search Page - Check search functions",
						"Message : Approach -" + option.getKey() + "is NOT Displayed - For Check search functions");
				rpt.Pass("Message : Approach -" + option.getKey() + " NOT Displayed - For Check search functions");
				rpt.Category("MB- Company Search Page - Check search functions");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				rpt.chkBugs(CommonUtils.getUserName());
			}
			browser.navigate().back();
			findTheElement("xpath=//*[@id=\"" + option.getValue()[0] + "\"]").clear();
		}
	}

	public void recentSearches() throws Exception {
		click("xpath=//*[@class=\"nav nav-list\"]/li[1]");
		if (findTheElement("xpath=//*[@class=\"nav nav-list\"]/li[2]").isDisplayed()) {
			click("xpath=//*[@class=\"nav nav-list\"]/li[2]");
			waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
			waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");
			rpt.createTest("MB- Company Search Page -Check Recent Search functionality",
					"Recent Search is working - For Check Recent Search functionality");
			rpt.Pass("Recent Search is working - For Check Recent Search functionality");
			rpt.Category("MB - Company Search Page -Check Recent Search functionality");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Recent Search functionality",
					"Recent Search is not working - For Check Recent Search functionality");
			rpt.Fail("Recent Search is not working - For Check Recent Search functionality");
			rpt.Category("MB- Company Search Page -Check Recent Search functionality");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		browser.navigate().back();
	}

	public void searchResultPages() throws Exception {
		// Advance_Search results page
		sendKeys("xpath=//*[@id=\"id_company_name\"]", "Amazon");// Search Box
		click("xpath=//*[@id=\"id_adv_search_type\"]");
		waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
		waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");
		Map<String, String> Elements = new HashMap<String, String>();
		Elements.put("Filter Options Element", "//*[@id=\"container-main\"]/div[1]");
		Elements.put("Results Info Element", "//*[@id=\"company_results_info\"]");
		Elements.put("Pagination", "//*[@class=\"dataTables_paginate paging_bootstrap pagination\"]");
		for (Map.Entry<String, String> Element : Elements.entrySet()) {
			if (findTheElement("xpath=" + Element.getValue() + "").isDisplayed()) {
				rpt.createTest("MB- Company Search Page -Check Database Search Result page Elements",
						"Message : Element -" + Element.getKey()
								+ " Displayed - For Checking Database Search Result page Elements");
				rpt.Pass("Message :Element -" + Element.getKey()
						+ " Displayed - For Checking Database Search Result page Elements");
				rpt.Category("MB- Company Search Page -Check Database Search Result page Elements");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				rpt.chkBugs(CommonUtils.getUserName());

			} else {
				rpt.createTest("MB- Company Search Page -Check Database Search Result page Elements",
						"Message : Element - " + Element.getKey()
								+ "is NOT Displayed - For Checking Database Search Result page Elements");
				rpt.Pass("Message : Element - " + Element.getKey()
						+ " NOT Displayed - For Checking Database Search Result page Elements");
				rpt.Category("MB- Company Search Page -Check Database Search Result page Elements");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				rpt.chkBugs(CommonUtils.getUserName());

			}
		}
		browser.navigate().back();
		// API_Search results page
		click("xpath=//*[@id=\"id_api_search_type\"]");
		waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
		waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");
		Map<String, String> API_Elements = new HashMap<String, String>();
		API_Elements.put("Filter Options Element", "//*[@id=\"container-main\"]/div[1]");
		API_Elements.put("Results Info Element", "//*[@id=\"company_results_info\"]");
		API_Elements.put("Pagination", "//*[@class=\"dataTables_paginate paging_bootstrap pagination\"]");
		for (Map.Entry<String, String> API_Element : API_Elements.entrySet()) {
			if (findTheElement("xpath=" + API_Element.getValue() + "").isDisplayed()) {
				rpt.createTest("MB- Company Search Page -Check API Search Result page Elements", "Message : Element -"
						+ API_Element.getKey() + " Displayed - For Checking API Search Result page Elements");
				rpt.Pass("Message :Element -" + API_Element.getKey()
						+ " Displayed - For Checking API Search Result page Elements");
				rpt.Category("MB- Company Search Page -Check API Search Result page Elements");
				String path = rpt.CaptureScreen(browser, "ValidMessage");
				rpt.imgPathPass(path);
				rpt.chkBugs(CommonUtils.getUserName());

			} else {
				rpt.createTest("MB- Company Search Page -Check API Search Result page Elements", "Message : Element - "
						+ API_Element.getKey() + "is NOT Displayed - For Checking API Search Result page Elements");
				rpt.Pass("Message : Element - " + API_Element.getKey()
						+ " NOT Displayed - For Checking API Search Result page Elements");
				rpt.Category("MB- Company Search Page -Check API Search Result page Elements");
				String path = rpt.CaptureScreen(browser, "InvalidMessage");
				rpt.imgPathFail(path);
				rpt.chkBugs(CommonUtils.getUserName());

			}
		}
		browser.navigate().back();
		click("xpath=//*[@id=\"id_adv_search_type\"]");
		waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
		waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");
	}

	// Database Search results page
	public void filtersInResultPage_databaseSearch() throws Exception {
		// country
		Select countryFilter = new Select(findTheElement("xpath=//*[@id=\"countrynav\"]"));
		countryFilter.selectByIndex(1);
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		String resultCountry = findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr[1]/td[7]").getText();
		if (countryFilter.getFirstSelectedOption().getText().contains(resultCountry)) {
			rpt.createTest("MB- Company Search Page -Check country filter option",
					"Country_filter is working - For Check country filter option");
			rpt.Pass("Country_filter is working - For Check country filter option");
			rpt.Category("MB - Company Search Page -Check country filter option");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check country filter option",
					"Country_filter is not working - For Check country filter option");
			rpt.Fail("Country_filter is not working - For Check country filter option");
			rpt.Category("MB- Company Search Page -Check Recent country filter option");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		// State
		Select stateFilter = new Select(findTheElement("xpath=//*[@id=\"statenav\"]"));
		stateFilter.selectByIndex(1);
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		String resultState = findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr/td[6]").getText();
		if (stateFilter.getFirstSelectedOption().getText().contains(resultState)) {
			rpt.createTest("MB- Company Search Page -Check state filter option",
					"State_filter is working - For Check state filter option");
			rpt.Pass("State_filter is working - For Check state filter option");
			rpt.Category("MB - Company Search Page -Check state filter option");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check state filter option",
					"State_filter is not working - For Check state filter option");
			rpt.Fail("State_filter is not working - For Check state filter option");
			rpt.Category("MB- Company Search Page -Check Recent state filter option");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		// City
		Select cityFilter = new Select(findTheElement("xpath=//*[@id=\"citynav\"]"));
		cityFilter.selectByIndex(1);
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		String resultCity = findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr/td[5]").getText();
		if (cityFilter.getFirstSelectedOption().getText().contains(resultCity)) {
			rpt.createTest("MB- Company Search Page -Check City filter option",
					"City_filter is working - For Check City filter option");
			rpt.Pass("City_filter is working - For Check City filter option");
			rpt.Category("MB - Company Search Page -Check City filter option");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check City filter option",
					"City_filter is not working - For Check City filter option");
			rpt.Fail("City_filter is not working - For Check City filter option");
			rpt.Category("MB- Company Search Page -Check Recent City filter option");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		// Location Type
		countryFilter.selectByIndex(0);
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/div/ul/li[2]/a");
		String selectedValue = findTheElement("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/div/ul/li[2]/a/label")
				.getText();
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/label");
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		String locType = findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr[1]/td[9]").getText();
		Thread.sleep(2000);
		if (selectedValue.contains(locType)) {
			rpt.createTest("MB- Company Search Page -Check Location_Type filter option",
					"Location_Type is working - For Check Location_Type filter option");
			rpt.Pass("Location_Type is working - For Check Location_Type filter option");
			rpt.Category("MB - Company Search Page -Check Location_Type filter option");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Location_Type filter option",
					"Location_Type is not working - For Check Location_Type filter option");
			rpt.Fail("Location_Type is not working - For Check Location_Type filter option");
			rpt.Category("MB- Company Search Page -Check Recent Location_Type filter option");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/div/ul/li[2]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/label");

		// Industry Code
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/div/ul/li[3]/a");
		selectedValue = findTheElement("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/div/ul/li[3]/a/label")
				.getText();
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/label");
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		if (findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr/td[13]").isDisplayed()) {
			rpt.createTest("MB- Company Search Page -Check Industry_code filter option",
					"Industry_code is working - For Check Industry_code filter option");
			rpt.Pass("Industry_code is working - For Check Industry_code filter option");
			rpt.Category("MB - Company Search Page -Check Industry_code filter option");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Industry_code filter option",
					"Industry_code is not working - For Check Industry_code filter option");
			rpt.Fail("Industry_code is not working - For Check Industry_code filter option");
			rpt.Category("MB- Company Search Page -Check Recent Industry_code filter option");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/div/ul/li[3]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/label");

		// Employees
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/div/ul/li[2]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/label");
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		if (findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr[1]/td[10]").isDisplayed()) {
			rpt.createTest("MB- Company Search Page -Check Employees_Filter filter option",
					"Employees_Filter is working - For Check Employees_Filter filter option");
			rpt.Pass("Employees_Filter is working - For Check Employees_Filter filter option");
			rpt.Category("MB - Company Search Page -Check Employees_Filter filter option");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Employees_Filter filter option",
					"Employees_Filter is not working - For Check Employees_Filter filter option");
			rpt.Fail("Employees_Filter is not working - For Check Employees_Filter filter option");
			rpt.Category("MB- Company Search Page -Check Recent Employees_Filter filter option");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/div/ul/li[2]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/label");
		// Download
		if (findTheElement("xpath=//*[@id=\"download\"]").isDisplayed()) {
			// click("xpath=//*[@id=\"download\"]");
			rpt.createTest("MB- Company Search Page -Check Download option",
					"Download is working - For Check Downloadr option");
			rpt.Pass("Download is working - For Check Download option");
			rpt.Category("MB - Company Search Page -Check Download option");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Download option",
					"Download is not working - For Check Download option");
			rpt.Fail("Download is not working - For Check Download option");
			rpt.Category("MB- Company Search Page -Check Recent Download option");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		// Pagination
		if (findTheElement("xpath=//*[@class=\"dataTables_paginate paging_bootstrap pagination\"]").isDisplayed()) {
			click("xpath=//*[@class=\"dataTables_paginate paging_bootstrap pagination\"]/ul/li[3]/a");
			rpt.createTest("MB- Company Search Page -Check Pagination", "Pagination is working - For Check Pagination");
			rpt.Pass("Pagination is working - For Check Pagination");
			rpt.Category("MB - Company Search Page -Check Pagination");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Pagination",
					"Pagination is not working - For Check Pagination");
			rpt.Fail("Pagination is not working - For Check Pagination");
			rpt.Category("MB- Company Search Page -Check Pagination");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}

		browser.navigate().back();
	}

	// Api Search Reults Page
	public void filtersINResultpage_apiSearch() throws Exception {
		click("xpath=//*[@id=\"id_api_search_type\"]");
		waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
		waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");
		Select countryFilter = new Select(findTheElement("xpath=//*[@id=\"countrynav\"]"));
		countryFilter.selectByIndex(7);
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		String resultCountry = findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr[1]/td[7]").getText();
		if (countryFilter.getFirstSelectedOption().getText().contains(resultCountry)) {
			rpt.createTest("MB- Company Search Page -Check country filter option - API",
					"Country_filter is working - For Check country filter option - API");
			rpt.Pass("Country_filter is working - For Check country filter option - API");
			rpt.Category("MB - Company Search Page -Check country filter option - API");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check country filter option - API",
					"Country_filter is not working - For Check country filter option - API");
			rpt.Fail("Country_filter is not working - For Check country filter option - API");
			rpt.Category("MB- Company Search Page -Check Recent country filter option - API");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		// State
		Select stateFilter = new Select(findTheElement("xpath=//*[@id=\"statenav\"]"));
		stateFilter.selectByIndex(1);
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		String resultState = findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr[1]/td[6]").getText();
		if (stateFilter.getFirstSelectedOption().getText().contains(resultState)) {
			rpt.createTest("MB- Company Search Page -Check state filter option - API",
					"State_filter is working - For Check state filter option- API");
			rpt.Pass("State_filter is working - For Check state filter option- API");
			rpt.Category("MB - Company Search Page -Check state filter option- API");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check state filter option- API",
					"State_filter is not working - For Check state filter option- API");
			rpt.Fail("State_filter is not working - For Check state filter option- API");
			rpt.Category("MB- Company Search Page -Check Recent state filter option- API");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		// City
		Select cityFilter = new Select(findTheElement("xpath=//*[@id=\"citynav\"]"));
		cityFilter.selectByIndex(1);
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		String resultCity = findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr[1]/td[5]").getText();
		Thread.sleep(2000);
		if (cityFilter.getFirstSelectedOption().getText().contains(resultCity)) {
			rpt.createTest("MB- Company Search Page -Check City filter option- API",
					"City_filter is working - For Check City filter option- API");
			rpt.Pass("City_filter is working - For Check City filter option- API");
			rpt.Category("MB - Company Search Page -Check City filter option- API");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check City filter option- API",
					"City_filter is not working - For Check City filter option- API");
			rpt.Fail("City_filter is not working - For Check City filter option- API");
			rpt.Category("MB- Company Search Page -Check Recent City filter option- API");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		// Location Type
		countryFilter.selectByIndex(0);
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/div/ul/li[2]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/label");
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}

		if (findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr[1]/td[9]").isDisplayed()) {
			rpt.createTest("MB- Company Search Page -Check Location_Type filter option- API",
					"Location_Type is working - For Check Location_Type filter option- API");
			rpt.Pass("Location_Type is working - For Check Location_Type filter option- API");
			rpt.Category("MB - Company Search Page -Check Location_Type filter option- API");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Location_Type filter option- API",
					"Location_Type is not working - For Check Location_Type filter option- API");
			rpt.Fail("Location_Type is not working - For Check Location_Type filter option- API");
			rpt.Category("MB- Company Search Page -Check Recent Location_Type filter option- API");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/div/ul/li[2]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[4]/label");

		// Industry Code
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/div/ul/li[3]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/label");
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		if (findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr[1]/td[13]").isDisplayed()) {
			rpt.createTest("MB- Company Search Page -Check Industry_code filter option- API",
					"Industry_code is working - For Check Industry_code filter option- API");
			rpt.Pass("Industry_code is working - For Check Industry_code filter option- API");
			rpt.Category("MB - Company Search Page -Check Industry_code filter option- API");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Industry_code filter option- API",
					"Industry_code is not working - For Check Industry_code filter option- API");
			rpt.Fail("Industry_code is not working - For Check Industry_code filter option- API");
			rpt.Category("MB- Company Search Page -Check Recent Industry_code filter option- API");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/div/ul/li[3]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[5]/label");

		// Employees
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/div/ul/li[2]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/label");
		click("xpath=//*[@id=\"navreload\"]");
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}
		if (findTheElement("xpath=//*[@id=\"company_results\"]/tbody/tr[1]/td[10]").isDisplayed()) {
			rpt.createTest("MB- Company Search Page -Check Employees_Filter filter option- API",
					"Employees_Filter is working - For Check Employees_Filter filter option- API");
			rpt.Pass("Employees_Filter is working - For Check Employees_Filter filter option- API");
			rpt.Category("MB - Company Search Page -Check Employees_Filter filter option- API");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Employees_Filter filter option- API",
					"Employees_Filter is not working - For Check Employees_Filter filter option- API");
			rpt.Fail("Employees_Filter is not working - For Check Employees_Filter filter option- API");
			rpt.Category("MB- Company Search Page -Check Recent Employees_Filter filter option- API");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/div/button");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/div/ul/li[2]/a");
		click("xpath=//*[@id=\"container-main\"]/div[1]/div[6]/label");
		// Download
		if (findTheElement("xpath=//*[@id=\"download\"]").isDisplayed()) {
			// click("xpath=//*[@id=\"download\"]");
			rpt.createTest("MB- Company Search Page -Check Download option- API",
					"Download is working - For Check Downloadr option- API");
			rpt.Pass("Download is working - For Check Download option- API");
			rpt.Category("MB - Company Search Page -Check Download option- API");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Download option- API",
					"Download is not working - For Check Download option- API");
			rpt.Fail("Download is not working - For Check Download option- API");
			rpt.Category("MB- Company Search Page -Check Recent Download option- API");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		// Pagination
		if (findTheElement("xpath=//*[@class=\"dataTables_paginate paging_bootstrap pagination\"]").isDisplayed()) {
			click("xpath=//*[@class=\"dataTables_paginate paging_bootstrap pagination\"]/ul/li[3]/a");
			rpt.createTest("MB- Company Search Page -Check Pagination- API",
					"Pagination is working - For Check Pagination- API");
			rpt.Pass("Pagination is working - For Check Pagination- API");
			rpt.Category("MB - Company Search Page -Check Pagination- API");
			String path = rpt.CaptureScreen(browser, "ValidMessage");
			rpt.imgPathPass(path);
			rpt.chkBugs(CommonUtils.getUserName());

		} else {
			rpt.createTest("MB- Company Search Page - Check Pagination- API",
					"Pagination is not working - For Check Pagination- API");
			rpt.Fail("Pagination is not working - For Check Pagination- API");
			rpt.Category("MB- Company Search Page -Check Pagination- API");
			String path = rpt.CaptureScreen(browser, "InvalidMessage");
			rpt.imgPathFail(path);
			rpt.chkBugs(CommonUtils.getUserName());
		}
		while (findTheElement("xpath=//*[@id=\"company_results_processing\"]").isDisplayed()) {
			Thread.sleep(5000);
		}

		browser.navigate().back();
		click("xpath=//*[@id=\"id_adv_search_type\"]");
		waitForPageElementToLoad("xpath=//*[@id=\"company_results\"]");
		waitForPageElementToBeDisplayed("xpath=//*[@id=\"company_results\"]/tbody");

	}

	public void downloadFunctions() throws Exception {
		DownloadFunctionality_MB download = new DownloadFunctionality_MB();
		download.verifyCompanyNamePageDownloadButton();
		download.verifyClickCompanyName();
		download.verifyPrintButtonInCompanyDetailPage();
		download.verifyAlternativeAndMinorityButtonInCompanyDetailPage();
		download.verifyDownloadButtonInCompanyDetailPage();
	}
}
