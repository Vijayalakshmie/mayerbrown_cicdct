package Test;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.MediaEntityModelProvider;


public class Report {

	private static ExtentReports extent;
	private static ExtentTest test;
	private static ExtentHtmlReporter htmlReporter;
		
	private static String filePath = "D:\\MB\\MB_Report\\myReport.html";
	public static WebDriver browser = BrowserFactory.getBrowser("Browser","URL");
	static String imagePath="D:\\MB\\MB_Screenshot\\";
	
		
	private static long counter =0;
	BrowserFactory br = new BrowserFactory();
	public static ExtentReports GetExtent(){
		if (extent != null)
                    return extent; //avoid creating new instance of html file
                extent = new ExtentReports();		
		extent.attachReporter(getHtmlReporter());
		
		return extent;
	}
 
	private static ExtentHtmlReporter getHtmlReporter() {
	
        htmlReporter = new ExtentHtmlReporter(filePath);
		
	// make the charts visible on report open
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setDocumentTitle("MAYER_BROWN");
        htmlReporter.config().setReportName("MAYER_BROWN: Regression Suite");
        return htmlReporter;
        
	}
		
	public static ExtentTest createTest(String name, String description){
		test = extent.createTest(name, description);
		return test;	
	}
	public static ExtentTest Pass(String Message)
	{
		test.pass(Message);
		test.assignCategory("MAYER_BROWN : Regression");
		return test;
	}
	public static  ExtentTest Fail(String FailMessage)
	{
		test.fail(FailMessage);
		test.assignCategory("MAYER_BROWN : Regression");
		test.warning(FailMessage);
		test.error(FailMessage);
		return test;
		
		
	}
	public static ExtentTest Skip(String SkipMessage)
	{
		test.skip(SkipMessage);
		test.assignCategory("MAYER_BROWN : Regression");
		
		return test;
	}

	public static ExtentTest Info(String InfoMessage)throws Exception
	{
		test.info(InfoMessage);
		return test;
	}
	public static ExtentTest imgPathPass(String path)throws Exception
	{
		test.pass("ScreenShot", MediaEntityBuilder.createScreenCaptureFromPath(path).build());
		return test;
		
	}
	public static ExtentTest imgPathFail(String path)throws Exception
	{
		test.fail("ScreenShot", MediaEntityBuilder.createScreenCaptureFromPath(path).build());
		return test;
		
		
	}
	
	public static ExtentTest Category(String Category)throws Exception
	{
		test.assignCategory(Category);
		return test;
	}
	
	public static ExtentTest chkBugs(String Bugs)throws Exception
	{
		test.assignAuthor(Bugs);
		return test;
	}
	public static ExtentTest chkdebug(String dbg)throws Exception
	{
		//test.debug(dbg);
		test.error(dbg);

		return test;
	}

	
		
	public static String CaptureScreen(WebDriver driver,String imgName)
	{

		TakesScreenshot oScn = (TakesScreenshot) driver;
		File oScnShot = oScn.getScreenshotAs(OutputType.FILE);
		String fileName = imagePath+imgName+(++counter)+".png";
		File oDest = new File(fileName);
		try
		{
			FileUtils.copyFile(oScnShot, oDest);
		} 
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
		return fileName;
	}
}
